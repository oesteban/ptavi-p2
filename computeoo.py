import math
import sys

default = 2

class Compute():
    def __init__(self):
        self.default = 2
        """Valor default"""

    def power(self, num, num2):
        return num ** num2

    def log(self,  num, num2):
        return math.log(num, num2)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    if len(sys.argv) == 4:
        num = int(sys.argv[2])
        num2 = int(sys.argv[3])
        objeto = Compute()
    
    if sys.argv[1] == "power":
        result = objeto.power(num, num2)
    elif sys.argv[1] == "log":
        result = objeto.log(num, num2)
    else:
        sys.exit('Operand should be power or log')

    print(result)