import sys
import computeoo

class ComputeChild(computeoo.Compute):
    
    def set_def(self):
        if len(sys.argv) == 4:
            self.default = int(sys.argv[3])
            if self.default <= 0:
                raise ValueError("No acepta base igual o menor a cero")     

    def get_def(self):
        return self.default

if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    if len(sys.argv) >= 3:
        objeto = ComputeChild()
        objeto.set_def()
        num = int(sys.argv[2])
        num2 = objeto.get_def()
    
    if sys.argv[1] == "power":
        result = objeto.power(num, num2)
    elif sys.argv[1] == "log":
        result = objeto.log(num, num2)
    else:
        sys.exit('Operand should be power or log')

    print(result)