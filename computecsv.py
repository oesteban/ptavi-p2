import sys
import computecount

compute = computecount.Compute()

def process_line(line):
    contents = line.rstrip().split(',')
    if len(contents) == 1:
        compute.default = int(contents[0])
        print("Default: " + (contents[0]))
    elif len(contents) == 2:
        if contents[0] == "power":
            print(compute.power(int(contents[1]), compute.default))
        elif contents[0] == "log":
            print(compute.log(int(contents[1]), compute.default))
        else:
            print("Bad format")

    elif len(contents) == 3:
        if contents[0] == "power":
            print(compute.power(int(contents[1]), int(contents[2])))
        elif contents[0] == "log":
            print(compute.log(int(contents[1]), int(contents[2])))
        else:
            print("Bad format")
    else:
        print("Bad format")

def process_csv(filename):
    with open(filename, 'r') as file:
        for line in file:
            try:
                process_line(line)
            except (ValueError, KeyError):
                print("Bad format")


if __name__ == "__main__":
    try:
        filename = sys.argv[1]
    except IndexError:
        print("Error: Execute with the name of file to process as argument")
        sys.exit(-1)
    try:
        process_csv(filename)
    except FileNotFoundError:
        print(f"Error: File {filename} does not exist")
        sys.exit(-1)
