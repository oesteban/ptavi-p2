import math
import sys

class Compute():
    def __init__(self):
        self.default = 2
        self.count = 0

    def power(self, num, num2):
        self.count += 1
        return num ** num2

    def log(self,  num, num2):
        self.count += 1
        return math.log(num, num2)
    
    def set_def(self):
        self.count += 1
        if len(sys.argv) == 4:
            self.default = int(sys.argv[3])
            if self.default <= 0:
                raise ValueError("No acepta base igual o menor a cero")     

    def get_def(self):
        self.count += 1
        return self.default